drop database productsdb;
create database productsdb with template=template0 owner=postgres;
\connect productsdb;
alter default privileges grant all on tables to postgres;
alter default privileges grant all on sequences to postgres;