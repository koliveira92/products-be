package com.products.productsbe.controllers;

import com.products.productsbe.domain.Product;
import com.products.productsbe.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestResource
public class ProductsController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping
    public Page<Product> findByName(@RequestParam("name") String name, Pageable pageable) {
        if (name == null) {
            return productRepository.findAll(pageable);
        } else {
            return productRepository.findByNameContainingIgnoreCase(name, pageable);
        }
    }
}

