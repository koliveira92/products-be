package com.products.productsbe.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer productId;

    private String name;

    private String description;

    private Double  priceFrom;

    private Double  priceFor;

    @OneToMany(targetEntity=Image.class, fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
    @JoinColumn(name="product_id")
    private List<Image> images;

    public Product() { }

    public Product(String name, String description, Double priceFrom, Double priceFor, List<Image> images) {
        this.name = name;
        this.description = description;
        this.priceFrom = priceFrom;
        this.priceFor = priceFor;
        this.images = images;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(Double priceFrom) {
        this.priceFrom = priceFrom;
    }

    public Double getPriceFor() {
        return priceFor;
    }

    public void setPriceFor(Double priceFor) {
        this.priceFor = priceFor;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
