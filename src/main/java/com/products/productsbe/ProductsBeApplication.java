package com.products.productsbe;

import com.products.productsbe.domain.Image;
import com.products.productsbe.domain.Product;
import com.products.productsbe.repositories.ProductRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@SpringBootApplication
public class ProductsBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsBeApplication.class, args);
	}

	@Bean
	ApplicationRunner init(ProductRepository repository) {
		return args -> {
			createProducts(repository);
		};
	}

	void createProducts(ProductRepository repository) {
		String[][] data = {
				{"Kit Bewsy", "Kit colcha cama casal Bewsy", "199.99", "99.99", "set-1"},
				{"Kit Grieger", "Kit colcha cama casal Grieger", "299.99", "199.99", "set-2"},
				{"Kit Mewrcik", "Kit colcha cama casal Mewrcik", "399.99", "299.99", "set-3"},
				{"Kit Leversha", "Kit colcha cama casal Leversha", "499.99", "399.99", "set-4"},
				{"Kit Meran", "Kit colcha cama casal Meran", "599.99", "499.99", "set-5"},

				{"Kit Elderton", "Kit colcha cama casal Elderton", "199.99", "99.99", "set-1"},
				{"Kit McGucken", "Kit colcha cama casal McGucken", "299.99", "199.99", "set-2"},
				{"Kit Savil", "Kit colcha cama casal Savil", "399.99", "299.99", "set-3"},
				{"Kit Tallboy", "Kit colcha cama casal Tallboy", "499.99", "399.99", "set-4"},
				{"Kit Volant", "Kit colcha cama casal Volant", "599.99", "499.99", "set-5"},

				{"Kit Tabourin", "Kit colcha cama casal Tabourin", "199.99", "99.99", "set-1"},
				{"Kit Pindred", "Kit colcha cama casal Pindred", "299.99", "199.99", "set-2"},
				{"Kit Derks", "Kit colcha cama casal Derks", "399.99", "299.99", "set-3"},
				{"Kit Kennaway", "Kit colcha cama casal Kennaway", "499.99", "399.99", "set-4"},
				{"Kit Derill", "Kit colcha cama casal Derill", "599.99", "499.99", "set-5"},

				{"Kit Filipovic", "Kit colcha cama casal Filipovic", "199.99", "99.99", "set-1"},
				{"Kit Jandel", "Kit colcha cama casal Jandel", "299.99", "199.99", "set-2"},
				{"Kit Knoton", "Kit colcha cama casal Knoton", "399.99", "299.99", "set-3"},
				{"Kit Lapsley", "Kit colcha cama casal Lapsley", "499.99", "399.99", "set-4"},
				{"Kit Chattell", "Kit colcha cama casal Chattell", "599.99", "499.99", "set-5"},

				{"Kit Boomes", "Kit colcha cama casal Boomes", "199.99", "99.99", "set-1"},
				{"Kit Bavidge", "Kit colcha cama casal Bavidge", "299.99", "199.99", "set-2"},
				{"Kit Williams", "Kit colcha cama casal Williams", "399.99", "299.99", "set-3"},
				{"Kit Mance", "Kit colcha cama casal Mance", "499.99", "399.99", "set-4"},
				{"Kit Snarr", "Kit colcha cama casal Snarr", "599.99", "499.99", "set-5"},

				{"Kit Emsley", "Kit colcha cama casal Emsley", "199.99", "99.99", "set-1"},
				{"Kit Pashe", "Kit colcha cama casal Pashe", "299.99", "199.99", "set-2"},
				{"Kit Turbitt", "Kit colcha cama casal Turbitt", "399.99", "299.99", "set-3"},
				{"Kit Buckerfield", "Kit colcha cama casal Buckerfield", "499.99", "399.99", "set-4"},
				{"Kit Mixture", "Kit colcha cama casal Mixture", "599.99", "499.99", "set-5"},

				{"Kit Maffiotti", "Kit colcha cama casal Maffiotti", "199.99", "99.99", "set-1"},
				{"Kit Suscens", "Kit colcha cama casal Suscens", "299.99", "199.99", "set-2"},
				{"Kit Wilkison", "Kit colcha cama casal Wilkison", "399.99", "299.99", "set-3"},
				{"Kit Buckeridge", "Kit colcha cama casal Buckeridge", "499.99", "399.99", "set-4"},
				{"Kit Bellerby", "Kit colcha cama casal Bellerby", "599.99", "499.99", "set-5"},

				{"Kit Flohard", "Kit colcha cama casal Flohard", "199.99", "99.99", "set-1"},
				{"Kit Raggitt", "Kit colcha cama casal Raggitt", "299.99", "199.99", "set-2"},
				{"Kit Winnard", "Kit colcha cama casal Winnard", "399.99", "299.99", "set-3"},
				{"Kit Crossland", "Kit colcha cama casal Crossland", "499.99", "399.99", "set-4"},
				{"Kit Golds", "Kit colcha cama casal Golds", "599.99", "499.99", "set-5"},

				{"Kit Archibald", "Kit colcha cama casal Archibald", "199.99", "99.99", "set-1"},
				{"Kit Hachette", "Kit colcha cama casal Hachette", "299.99", "199.99", "set-2"},
				{"Kit Kienl", "Kit colcha cama casal Kienl", "399.99", "299.99", "set-3"},
				{"Kit Legrave", "Kit colcha cama casal Legrave", "499.99", "399.99", "set-4"},
				{"Kit Pryor", "Kit colcha cama casal Pryor", "599.99", "499.99", "set-5"},

				{"Kit Dundendale", "Kit colcha cama casal Dundendale", "199.99", "99.99", "set-1"},
				{"Kit Linacre", "Kit colcha cama casal Linacre", "299.99", "199.99", "set-2"},
				{"Kit Clogg", "Kit colcha cama casal Clogg", "399.99", "299.99", "set-3"},
				{"Kit Biasi", "Kit colcha cama casal Biasi", "499.99", "399.99", "set-4"},
				{"Kit Roney", "Kit colcha cama casal Roney", "599.99", "499.99", "set-5"}
		};

		Stream.of(data).forEach(array -> {
			try {
				Product product = new Product(array[0],	array[1], Double.parseDouble(array[2]),	Double.parseDouble(array[3]), getListOfImage(array[4]));
				repository.save(product);

			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		});
	}

	List<Image> getListOfImage(String set) {
		switch (set) {
			case "set-1":
				return Arrays.asList(
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/c/o/colcha-casal-dupla-face-casa-riachuelo-ariel-branco-14045621_foto1_frontal.jpg"),
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/c/o/colcha-casal-dupla-face-casa-riachuelo-ariel-branco-14045621_foto2_costas.jpg"),
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/c/o/colcha-casal-dupla-face-casa-riachuelo-ariel-branco-14045621_foto3_lateral.jpg")
				);
			case "set-2":
				return Arrays.asList(
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/k/i/kit-colcha-design-cross-ak-home-special-branco-casal-queen-13974033_foto1_frontal.jpg"),
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/k/i/kit-colcha-design-cross-ak-home-special-branco-casal-queen-13974033_foto2_costas.jpg"),
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/k/i/kit-colcha-design-cross-ak-home-special-branco-casal-queen-13974033_foto3_lateral.jpg")
				);
			case "set-3":
				return Arrays.asList(
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/k/i/kit-colcha-design-pompom-ak-home-special-azul-marinho-casal-queen-13974025_foto1_frontal.jpg"),
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/k/i/kit-colcha-design-pompom-ak-home-special-azul-marinho-casal-queen-13974025_foto2_costas.jpg")
				);
			case "set-4":
				return Arrays.asList(
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/k/i/kit-colcha-design-frills-ak-home-special-rosa-claro-solteiro-13973959_foto1_frontal.jpg"),
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/k/i/kit-colcha-design-frills-ak-home-special-rosa-claro-solteiro-13973959_foto2_costas.jpg")
				);
			case "set-5":
				return Arrays.asList(
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/k/i/kit-colcha-design-rice-ak-home-special-preto-casal-queen-13974041_foto1_frontal.jpg"),
						new Image("https://produtos.fotos-riachuelo.com.br/media/catalog/product/cache/3541e153ef6ead3044d72626c3847968/k/i/kit-colcha-design-rice-ak-home-special-preto-casal-queen-13974041_foto2_costas.jpg")
				);
			default:
				return Arrays.asList();
		}
	}
}
