# Product BE

Backend project for a simple Products List Page

## Setup and Installation

Clone the repository

```sh
$ git clone https://koliveira92@bitbucket.org/koliveira92/products-be.git
$ cd products-be
```

### Setup the database

**(Important)** Make sure you have Docker installed.

First, start the database conteiner:

```sh
$ docker-compose up -d
```

After copy this file to the running container using below command and then exec into the running container:

```sh
$ docker container cp productsdb.sql postgres_container:/
$ docker container exec -it postgres_container bash
```

Run the script using psql client:

```sh
$ psql -U postgres --file productsdb.sql
```

To exit from container:

```sh
$ exit
```

### Running Locally

**(Important)** Make sure you have Java and Maven installed.

Install Maven dependencies and run the project

```sh
$ mvn install
$ ./mvnw spring-boot:run
```
Your app should now be running on [localhost:8080](http://localhost:8080/).

### Examples

Some examples of request:

[localhost:8080/products/](http://localhost:8080/products/)

[localhost:8080/products/?page=0&size=20](http://localhost:8080/products/?page=0&size=20)

[localhost:8080/products/search/findByNameContainingIgnoreCase?name=wil](http://localhost:8080/products/search/findByNameContainingIgnoreCase?name=wil)

[localhost:8080/products/1](http://localhost:8080/products/1)
